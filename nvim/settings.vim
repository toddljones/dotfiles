" Compatibility
" =============

" enter the current millenium
set nocompatible


" Appearance
" ==========

syntax enable
set t_Co=256

set background=light
colorscheme solarized

set ruler showcmd
set number relativenumber


" Search
" ======

set ignorecase smartcase incsearch


" Whitespace
" ==========

set listchars=tab:▸\ ,space:·,nbsp:~,eol:$


" Wrapping
" ========

set linebreak

" Read more at http://vimcasts.org/episodes/formatting-text-with-par/
set formatprg:par\ -rq


" Diff
" ====

set diffopt+=vertical

" Undo
" ====

set undofile                     " save undos after file closes
set undodir=~/dotfiles/nvim/undo " where to save undo histories
set undolevels=1000              " how many undos
set undoreload=10000             " number of lines to save for undo

" Completion
" ==========

set complete+=kspell

" Finding files
" =============

set path+=**
