call plug#begin('~/dotfiles/nvim/plugged')

    " UI
    " ==

    Plug 'altercation/vim-colors-solarized'


    " Workflow
    " ========

    Plug 'tpope/vim-repeat'
    Plug 'tpope/vim-unimpaired'
    Plug 'nelstrom/vim-visual-star-search'
    Plug 'tpope/vim-abolish'


    " Error checking
    " ==============

    Plug 'neomake/neomake'


    " Tools
    " =====

    Plug 'vimwiki/vimwiki'
    Plug 'godlygeek/tabular'


    " Integration
    " ===========

    Plug 'tpope/vim-fugitive' " Git
    Plug 'lervag/vimtex'      " LaTeX
    Plug 'plasticboy/vim-markdown'


    " Experimental
    " ============

    Plug 'nelstrom/vim-americanize' " UK -> US English auto-corrections
    " depends on tpope/vim-abolish
    
    Plug 'dhruvasagar/vim-table-mode'
    " Plug 'SirVer/ultisnips'
    
    " Plug 'easymotion/vim-easymotion'


call plug#end()
