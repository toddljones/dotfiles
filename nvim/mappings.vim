" Whitespace
" ==========

nmap _$ :StripTrailingWhitespace<CR>
nmap _= :call Preserve("normal gg=G")<CR>


" Buffers
" =======

nnoremap <C-p> :buffer#<CR>
" ^p defaults to k


" Windows
" =======

map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l


" Insert mode
" ===========

inoremap kj <Esc>


" Seach
" =====

nmap <Esc> :noh<CR>

nmap <F6> :exec '!'.getline('.')<CR>

" Word Count
" ==========

" Latex word count
nmap <F3> :w !detex \| wc -w<CR>

