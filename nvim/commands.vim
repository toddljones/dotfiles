" Save current position before executing command
" ==============================================

" source: http://vimcasts.org/episodes/tidying-whitespace/
function! Preserve(command)
  " Preparation: save last search, and cursor position.
  let _s=@/
  let l = line(".")
  let c = col(".")
  " Do the business:
  execute a:command
  " Clean up: restore previous search history, and cursor position
  let @/=_s
  call cursor(l, c)
endfunction 


" Set tabstop, softtabstop and shiftwidth to the same value
" =========================================================

" source: http://vimcasts.org/episodes/tabs-and-spaces/
command! -nargs=* Stab call Stab()
function! Stab()
  let l:tabstop = 1 * input('set tabstop = softtabstop = shiftwidth = ')
  if l:tabstop > 0
    let &l:sts = l:tabstop
    let &l:ts = l:tabstop
    let &l:sw = l:tabstop
  endif
  call SummarizeTabs()
endfunction

function! SummarizeTabs()
  try
    echohl ModeMsg
    echon 'tabstop='.&l:ts
    echon ' shiftwidth='.&l:sw
    echon ' softtabstop='.&l:sts
    if &l:et
      echon ' expandtab'
    else
      echon ' noexpandtab'
    endif
  finally
    echohl None
  endtry
endfunction


" Whitespace
" ==========

" source: http://vimcasts.org/episodes/tidying-whitespace/
command! -nargs=* StripTrailingWhitespace call Preserve("%s/\\s\\+$//e")

" source: http://vimcasts.org/episodes/soft-wrapping-text/
command! -nargs=* Wrap set wrap linebreak nolist

command! -nargs=* Justify setlocal formatprg+=j
command! -nargs=* NoJustify setlocal formatprg-=j


" Set spelling language
" =====================

command! -nargs=* English set spelllang=en
command! -nargs=* EnglishBritish set spelllang=en_gb
command! -nargs=* EnglishAmerican set spelllang=en_us


" Changing vim config settings
" ============================

command! -nargs=* SourceVimConfig source $MYVIMRC
command! -nargs=* OpenVimConfig tabedit $MYVIMRC | cd ~/dotfiles
