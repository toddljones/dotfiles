" vimwiki config
" ==============

let g:vimwiki_global_ext = 0
let g:vimwiki_list = [{
        \'path': '~/wiki/personal/',
        \'template_path': '~/wiki/setup/',
        \'template_default': 'default',
        \'template_ext': '.tpl',
        \'syntax': 'markdown',
        \'ext': '.md',
        \'path_html': '~/wiki/html/personal/',
        \'css_file': '~/wiki/setup/style.css',
        \'custom_wiki2html': 'vimwiki_markdown',
    \},
    \{
        \'path': '~/wiki/tech/',
        \'template_path': '~/wiki/setup/',
        \'template_default': 'default',
        \'template_ext': '.tpl',
        \'syntax': 'markdown',
        \'ext': '.md',
        \'path_html': '~/wiki/html/tech/',
        \'css_file': '~/wiki/setup/style.css',
        \'custom_wiki2html': 'vimwiki_markdown',
    \},
    \{
        \'path': '~/wiki/uni', 
        \'template_path': '~/wiki/setup/',
        \'template_default': 'default',
        \'template_ext': '.tpl',
        \'syntax': 'markdown',
        \'ext': '.md',
        \'path_html': '~/wiki/html/uni/',
        \'css_file': '~/wiki/setup/style.css',
        \'custom_wiki2html': 'vimwiki_markdown',
    \},
    \{
        \'path': '~/wiki/food/',
        \'template_path': '~/wiki/setup/',
        \'template_default': 'default',
        \'template_ext': '.tpl',
        \'syntax': 'markdown',
        \'ext': '.md',
        \'path_html': '~/wiki/html/food/',
        \'css_file': '~/wiki/setup/style.css',
        \'custom_wiki2html': 'vimwiki_markdown',
    \}]


" plasticboy/vim-markdown config
" ==============================

let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_new_list_item_indent = 0
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_fenced_languages = ['bash=sh']

" vimtex
" ======

let g:vimtex_latexmk_build_dir = "./out"
let g:tex_flavor='latex'
