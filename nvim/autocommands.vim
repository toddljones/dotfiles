if has("autocmd")

    " Whitespace
    " ==========

    " Filetypes that are particular about whitespace
    autocmd FileType make setlocal ts=4 sts=4 sw=4 noexpandtab
    autocmd FileType yaml setlocal ts=4 sts=4 sw=4 expandtab

    " Personal customisations
    autocmd FileType css,html,scss,vimwiki setlocal ts=2 sts=2 sw=2 expandtab
    autocmd FileType markdown,vim,tex setlocal ts=4 sts=4 sw=4 expandtab


    " Wrapping
    " ========

    autocmd FileType markdown,txt Wrap


    " Load spelling files
    " ===================

    autocmd VimEnter mkspell ~/dotfiles/nvim/spell/en.utf-8.add

    " Error checking
    " ==============

    autocmd! BufWritePost * Neomake
    autocmd FileType gitcommit setlocal spell

endif
