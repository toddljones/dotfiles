source ~/dotfiles/nvim/plugins.vim
source ~/dotfiles/nvim/commands.vim
source ~/dotfiles/nvim/settings.vim
source ~/dotfiles/nvim/variables.vim
source ~/dotfiles/nvim/mappings.vim
source ~/dotfiles/nvim/autocommands.vim
