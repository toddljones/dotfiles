########################################################################
## README                                                             ##
########################################################################

# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples


########################################################################
## WHEN TO LOAD                                                       ##
########################################################################

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac


########################################################################
## HISTORY                                                            ##
########################################################################

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000


########################################################################
## WINDOW                                                             ##
########################################################################

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize


########################################################################
## COLOR                                                              ##
########################################################################

# Set directory colors
eval "$(dircolors ~/dotfiles/bash/colors/solarized/dircolors-solarized/dircolors.256dark)"

export TERM=xterm-256color        # for common 256 color terminals (e.g. gnome-terminal)
#export TERM=screen-256color       # for a tmux -2 session (also for screen)
#export TERM=rxvt-unicode-256color # for a colorful rxvt unicode session


########################################################################
## ALIAS                                                              ##
########################################################################

. ~/dotfiles/bash/aliases.bash


########################################################################
## EDITOR                                                             ##
########################################################################

# Set editors.
export EDITOR=nvim
export VISUAL=nvim


########################################################################
## ALERT                                                              ##
########################################################################

# Use: sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'


########################################################################
## PROMPT                                                             ##
########################################################################

PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '


########################################################################
## START UP SCREEN                                                    ##
########################################################################

if [ -f ~/dotfiles/bash/entry-text.txt ]
    then
        cat ~/dotfiles/bash/entry-text.txt
fi


########################################################################
## AUTOCOMPLETION                                                     ##
########################################################################

if [ -f /etc/bash_completion ]; then
 . /etc/bash_completion
fi
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
