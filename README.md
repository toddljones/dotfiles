# Dotfiles for Todd Liebenschutz-Jones

## Installation

Note that gitconfig is copied because it may be removed while performing
operations on this directory, so a symlink may break.

~~~shell
git submodule init
git submodule update
sudo apt-get install par
sudo easy_install3 pip
pip install neovim
ln -s ~/dotfiles/nvim ~/.config/nvim
ln -s ~/dotfiles/bashrc ~/.bashrc
cp ~/dotfiles/gitconfig ~/.gitconfig
ln -s ~/dotfiles/latexmkrc ~/.latexmkrc
cp ~/dotfiles/.config/user-dirs.dirs ~/.config/user-dirs.dirs
~~~

* Should we use pip or pip3?

### Other installations

~~~shell
pip install proselint
gem install mdl
~~~

### Color scheme

For the bash solarized color scheme, run the install as follows.

~~~shell
sudo apt-get install dconf-cli
~/dotfiles/bash/colors/solarized/gnome-terminal-colors-solarized/install.sh
~~~

Choose [2] dark_alternative.

### Optional

To add introductory text for the terminal, link the file as follows.

~~~shell
ln -s TARGET_PATH ~/dotfiles/bash/entry-text.txt
~~~

## License

The MIT License (MIT)

Copyright © 2016 Todd Liebenschutz-Jones

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
